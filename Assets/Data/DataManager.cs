using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "DataManager", menuName = "DataManager")]
public class DataManager : ScriptableObject
{
    public List<Color> bothColors;
    public List<Color> gameColors;
    public List<Color> animeColors;

    public List<Color> GetTop3Color(WorkType type) 
    {
        List<Color> tempList = GetTypeOfColors(type);
        RefreshList<Color>(tempList, 6);
        tempList =  tempList.OrderByDescending(x => x.count).ToList();

        return new List<Color>() { tempList[0], tempList[1] , tempList[2] };
    }

    private List<Color> GetTypeOfColors(WorkType type)
    {
        if (type == WorkType.Anime)
        {
            return animeColors;
        }
        else if (type == WorkType.Game)
        {
            return gameColors;
        }
        else if (type == WorkType.Both)
        {
            return bothColors;
        }
        else
        {
            Debug.Log("colors of type： " + type.ToString() + "not found");
            return null;
        }
    }

    public List<Color> GetTop3ColorSortByWorksCount(WorkType type)
    {
        List<Color> tempList = GetTop3Color(type);
        if (type == WorkType.Anime)
        {
            return tempList.OrderBy(x => x.animeList.Count).ToList();
        }
        else if (type == WorkType.Game)
        {
            return tempList.OrderBy(x => x.gameList.Count).ToList();
        }
        else if (type == WorkType.Both)
        {
            return tempList.OrderBy(x => x.animeList.Count + x.gameList.Count).ToList();
        }
        else
        {
            Debug.Log("GetTop3ColorSortByWorksCount： " + type.ToString() + "not found");
            return null;
        }
    }

    private List<T> RefreshList<T>(List<T> list, int randomness)
    {
        for(int docount = 0; docount < randomness; docount++)
        {
            int i = docount % list.Count;
            int r =  Random.Range(0, list.Count);
            if (r == i)
            {
                r = (r+1) % list.Count;
            }
            var t = list[i];
            list[i] = list[r];
            list[r] = t;
        }
        return list;
    }

    public List<Work> GetRecommendWork(List<Color> colors, WorkType workType)
    {
        List<Work> allList = new List<Work>();
        for (int i = 0; i < colors.Count; i++)
        {
            if (workType == WorkType.Anime)
            {
                allList = colors[0].animeList.Union(colors[1].animeList).Union(colors[2].animeList).ToList();
            }
            else if (workType == WorkType.Game)
            {
                allList = colors[0].gameList.Union(colors[1].gameList).Union(colors[2].gameList).ToList();
            }
            else if (workType == WorkType.Both)
            {
                allList = colors[0].animeList.Union(colors[1].animeList).Union(colors[2].animeList).Union(colors[0].gameList).Union(colors[1].gameList).Union(colors[2].gameList).ToList();
            } 
            else
            {
                Debug.Log("alllist of type： " + workType.ToString() + "not found");
                return null;
            }
        }


        List<Work> workList = new List<Work>();
        if(workType == WorkType.Anime)
        {
            foreach(var color in colors)
            {
                List<Work> temp = color.animeList.Except(workList).ToList();
                if(temp.Count == 0)
                {
                    temp =  allList.Except(workList).ToList();
                }
                int r = Random.Range(0, temp.Count);
                workList.Add(temp[r]);
            }
        }
        else if(workType == WorkType.Game)
        {
            foreach (var color in colors)
            {
                List<Work> temp = color.gameList.Except(workList).ToList();
                if (temp.Count == 0)
                {
                    temp = allList.Except(workList).ToList();
                }
                int r = Random.Range(0, temp.Count);
                workList.Add(temp[r]);
            }
        }
        else
        {
            foreach (var color in bothColors)
            {
                List<Work> temp = color.animeList.Union(color.gameList).ToList().Except(workList).ToList();
                if (temp.Count == 0)
                {
                    temp = allList.Except(workList).ToList();
                }
                int r = Random.Range(0, temp.Count);
                workList.Add(temp[r]);
            }
        }
        return workList;
    }

    public void ResetAllColor()
    {
        foreach(Color color in bothColors)
            color.Reset();
    }
}
