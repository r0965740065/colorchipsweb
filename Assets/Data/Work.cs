using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Work", menuName = "Work")]
public class Work : ScriptableObject
{
    public new string name;
    public Sprite result;
}
