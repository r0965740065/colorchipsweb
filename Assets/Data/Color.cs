using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Color", menuName = "Color")] 
public class Color : ScriptableObject
{
    [SerializeField]public int count = 0;
    public Sprite sprite;
    public List<Work> gameList = new List<Work>();
    public List<Work> animeList = new List<Work>();


    public void Reset()
    {
        count = 0;
    }
    public void OnValueChange(bool b)
    {
        count = b ? count+= 1 : count-= 1;
    }
}
