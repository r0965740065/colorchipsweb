using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum WorkType
{
    Anime, Game, Both
}

public class Result : MonoBehaviour
{
    public RectTransform blendPanel;
    public DataManager dataManager;
    public WorkType workType;
    public Image blend;
    private Material m;

    [Header("���O")]
    public GameObject startPanel;
    public GameObject Q1Panel;
    public GameObject animePanel;
    public GameObject gamePanel;
    public GameObject resultPanel;

    [Header("���G")]
    public Image color01;
    public Image color02;
    public Image color03;

    public Image work01;
    public Image work02;
    public Image work03;

    private bool lookedAll;
    private bool resultClicked;

    public AudioSource liquidDrop;

    private GameObject current;
    private GameObject next;

    private void Start()
    {
        m = blend.material;
        dataManager.ResetAllColor();
    }

    public void SelectAnime()
    {
        workType = WorkType.Anime;
    }
    public void SelectGame()
    {
        workType = WorkType.Game;
    }
    public void SelectBoth()
    {
        workType = WorkType.Both;
    }

    public void NextPanel()
    {
        if(workType == WorkType.Anime)
        {
            animePanel.SetActive(true);
        }
        else if(workType == WorkType.Game)
        {
            gamePanel.SetActive(true);
        }
        else
        {
            animePanel.SetActive(true);
        }
    }

    public void LookResult()
    {
        if (resultClicked == true) return;

        if (workType == WorkType.Both && lookedAll == false)
        {
            AnimeToGame();
            lookedAll = true;
        }
        else
        {
            resultClicked = true;
            blendPanel.gameObject.SetActive(true);
            blendPanel.DOAnchorPosY(-4000, 2.75f).SetEase(Ease.InCubic).OnUpdate(() =>
            {
                if (blendPanel.anchoredPosition.y < -2000)
                {
                    if (gamePanel.activeInHierarchy == true)
                        gamePanel.SetActive(false);
                    else if(animePanel.activeInHierarchy == true)
                        animePanel.SetActive(false);
                }
            }).OnComplete(() =>
            {
                blendPanel.gameObject.SetActive(false);
            });
            liquidDrop.Play();
            Invoke("SetResult", 2f);
        }
    }

    public void StartToQ1()
    {
        current = startPanel;
        next = Q1Panel;
        StartBlend();
    }
    public void Q1ToAnime()
    {
        current = Q1Panel;
        next = animePanel;
        StartBlend();
    }
    public void Q1ToGame()
    {
        current = Q1Panel;
        next = gamePanel;
        StartBlend();
    }
    private void AnimeToGame()
    {
        current = animePanel;
        next = gamePanel;
        StartBlend();
    }

    private void StartBlend()
    {
        blend.gameObject.SetActive(true);
        blend.DOFade(1, 0.5f).From(0).OnComplete(() =>
        {
            m.SetFloat("_BGCtrl", 1);
            current.SetActive(false);
            next.SetActive(true);
            blend.DOFade(0, 0.5f).From(1).SetDelay(0.5f).OnComplete(() =>
            {
                m.SetFloat("_BGCtrl", 0);
                blend.gameObject.SetActive(false);
            });
        });
    }

    private void SetResult()
    {
        resultPanel.SetActive(true);
        List<Color> colors = dataManager.GetTop3ColorSortByWorksCount(workType);
        List<Work> works = dataManager.GetRecommendWork(colors, workType);

        color01.sprite = colors[0].sprite;
        color02.sprite = colors[1].sprite;
        color03.sprite = colors[2].sprite;

        work01.sprite = works[0].result;
        work02.sprite = works[1].result;
        work03.sprite = works[2].result;
    }
}
