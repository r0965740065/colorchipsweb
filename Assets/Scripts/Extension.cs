using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extension
{
    public static List<T> GetRandom<T>(this List<T> nums, int count)
    {
        if (count > nums.Count)
        {
            Debug.LogError("");
            return null;
        }

        List<T> result = new List<T>();
        List<int> id = new List<int>();

        for (int i = 0; i < nums.Count; i++)
        {
            id.Add(i);
        }

        int r;
        while (id.Count > nums.Count - count)
        {
            r = Random.Range(0, id.Count);
            result.Add(nums[id[r]]);
            id.Remove(id[r]);
        }
        return result;
    }
}
