using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RandomInk : MonoBehaviour
{
    public Color color;
    private Image image;

    public float fadeSec = 0.2f;
    //public AnimationCurve fadeCurve;
    public Vector2 scale = new Vector2(1f, 1.75f);
    private Tween fadeTween;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    public void OnValueChange(bool value)
    {
        if (color != null)
            color.OnValueChange(value);
        else
            Debug.Log(transform.parent.parent.name);
        if (fadeTween != null)
            fadeTween.Kill();
        fadeTween = image.DOFade(value ? 1 : 0, fadeSec).SetEase(Ease.Linear);

        if (value == false) return;
        transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
        transform.localScale = Vector3.one * Random.Range(scale.x, scale.y);
    }
}
