using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ContentArrow : MonoBehaviour
{
    public RectTransform rect;
    public GameObject arrow;
    public float Y;
    private Image arrowImage;

    private bool isOpen;
    private Tween fadeTween;

    private void Start()
    {
        isOpen = true;
        arrowImage = arrow.GetComponent<Image>();
    }

    private void Update()
    {
        if (rect.anchoredPosition.y > Y && isOpen == true)
        {
            isOpen = false;
            if (fadeTween != null)
                fadeTween.Kill();
            fadeTween = arrowImage.DOFade(0, 0.15f).OnComplete(() =>
            {
                arrow.gameObject.SetActive(false);
            });
        }
        else if (rect.anchoredPosition.y < Y && isOpen == false)
        {
            isOpen = true;
            arrow.gameObject.SetActive(true);
            if (fadeTween != null)
                fadeTween.Kill();
            fadeTween = arrowImage.DOFade(1, 0.15f);
        }
    }
}
